/home/maxi/Documentos/TDR/sims/simTest/Allclean
cd ${0%/*} || exit 1
. $WM_PROJECT_DIR/bin/tools/RunFunctions

restore0Dir
runApplication blockMesh
runApplication snappyHexMesh -overwrite
runApplication topoSet
runApplication createPatch -overwrite

touch block.foam
